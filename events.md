---
layout: default
title: Events
permalink: /events/
order: 60
---

# Events

Events are organized to exchange ideas about reproducible builds, get
a better understanding or cooperate on improving code or specifications.

**Unfortunately, due to the unprecedented events in 2020, there will be no in-person event this year.**

{% assign sorted_events = site.events | sort: 'event_date' | reverse %}
{% for page in sorted_events %}
{% if page.event_date_string and page.event_hide != true %}
## {{ page.title }}

*{{ page.event_date_string }}* — {{ page.event_location }}

{{ page.event_summary }}

[Read more…]({{ page.permalink | relative_url }})
{% endif %}
{% endfor %}
